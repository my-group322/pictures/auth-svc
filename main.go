package main

import (
	"os"

	"gitlab.com/my-group322/pictures/auth-svc/internal/app"
)

func main() {
	if !app.Start() {
		os.Exit(1)
	}
}
