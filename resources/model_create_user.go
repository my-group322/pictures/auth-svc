package resources

import jares "gitlab.com/my-group322/json-api-shared"

type CreateUser struct {
	jares.Key
	Attributes CreateUserAttributes `json:"attributes"`
}

type CreateUserAttributes struct {
	Email     string `json:"email"`
	Password  string `json:"password"`
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
}

type CreateUserResponse struct {
	Data     CreateUser     `json:"data"`
	Included jares.Included `json:"included"`
}

type CreateUserListResponse struct {
	Data     []CreateUser   `json:"data"`
	Included jares.Included `json:"included"`
}
