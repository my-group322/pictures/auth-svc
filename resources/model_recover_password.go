package resources

import jares "gitlab.com/my-group322/json-api-shared"

type RecoverPassword struct {
	jares.Key
	Attributes RecoverPasswordAttributes `json:"attributes"`
}

type RecoverPasswordAttributes struct {
	NewPassword string `json:"new_password"`
}

type RecoverPasswordResponse struct {
	Data     RecoverPassword `json:"data"`
	Included jares.Included  `json:"included"`
}

type RecoverPasswordListResponse struct {
	Data     []RecoverPassword `json:"data"`
	Included jares.Included    `json:"included"`
}
