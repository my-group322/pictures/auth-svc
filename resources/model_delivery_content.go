package resources

type DeliveryContent struct {
	Destination  string `json:"destination"`
	ReceiverName string `json:"receiver_name"`
	Link         string `json:"link"`
}
