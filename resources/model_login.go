package resources

import jares "gitlab.com/my-group322/json-api-shared"

type Login struct {
	jares.Key
	Attributes LoginAttributes `json:"attributes"`
}

type LoginAttributes struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}
