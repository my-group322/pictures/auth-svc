FROM golang:1.16-alpine

WORKDIR /go/src/my-group322/pictures/auth-svc
COPY . .
RUN CGO_ENABLED=0 GOOS=linux go build -o /usr/local/bin/auth-svc gitlab.com/my-group322/pictures/auth-svc

FROM alpine:3.9

COPY --from=0 /usr/local/bin/auth-svc /usr/local/bin/auth-svc

ENTRYPOINT ["auth-svc"]