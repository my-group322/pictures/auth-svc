package config

import (
	"github.com/kelseyhightower/envconfig"
	"github.com/sirupsen/logrus"
)

type Config struct {
	Port                  string `required:"true" envconfig:"PORT"`
	DBUrl                 string `required:"true" envconfig:"DB_URL"`
	RabbitMQUrl           string `required:"true" envconfig:"RABBIT_MQ_URL"`
	RedisUrl              string `required:"true" envconfig:"REDIS_URL"`
	PicsSvcUrl            string `required:"true" envconfig:"PICS_SVC_URL"`
	WebClientUrl          string `required:"true" envconfig:"WEB_CLIENT_URL"`
	JwtSecret             string `envconfig:"JWT_SECRET"`
	InternalAccessKey     string `envconfig:"INTERNAL_ACCESS_KEY"`
	TokenCleanerDisabled  bool   `envconfig:"TOKEN_CLEANER_DISABLED"`
	NotificationsDisabled bool   `envconfig:"NOTIFICATIONS_DISABLED"`
	NewRelicLicenceKey    string `envconfig:"NEW_RELIC_LICENSE_KEY"`
}

func New(log *logrus.Logger) (*Config, error) {
	var config Config

	if err := envconfig.Process("", &config); err != nil {
		return nil, err
	}

	if config.JwtSecret == "" {
		log.Warn("You are starting with empty jwt-secret value")
	}

	if config.TokenCleanerDisabled {
		log.Warn("Token cleaner disabled")
	}

	if config.NotificationsDisabled {
		log.Warn("Notifications disabled")
	}

	return &config, nil
}
