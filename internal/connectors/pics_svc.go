package connectors

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"

	newrelic "github.com/newrelic/go-agent"

	"gitlab.com/my-group322/pictures/pics-svc/resources"

	client "github.com/ilyasiv2003/go-http-client"
	"github.com/pkg/errors"
)

//go:generate mockery --case underscore --name Connector
type Connector interface {
	CreateUser(ctx context.Context, user *resources.UserResponse) error
	GetUser(ctx context.Context, userID string) (*resources.User, error)
}

type connector struct {
	client    client.Client
	accessKey string
}

func NewConnector(cli client.Client, accessKey string) Connector {
	return connector{cli, accessKey}
}

func (c connector) CreateUser(ctx context.Context, user *resources.UserResponse) error {
	request, err := c.client.PostWith("/users", user)
	if err != nil {
		return err
	}

	request.Header.Set("Authorization", c.accessKey)

	seg := newrelic.StartSegment(newrelic.FromContext(ctx), "PicsCreateUserCall")
	defer func(seg *newrelic.Segment) {
		_ = seg.End()
	}(seg)

	response, err := c.client.Do(ctx, request)
	if err != nil {
		return err
	}

	if response.Get().StatusCode != http.StatusCreated {
		return errors.New(fmt.Sprintf("got unsuccessful code: %d", response.Get().StatusCode))
	}

	return nil
}

func (c connector) GetUser(ctx context.Context, userID string) (*resources.User, error) {
	request, err := c.client.Get(fmt.Sprintf("/users/%s", userID))
	if err != nil {
		return nil, err
	}

	request.Header.Set("Authorization", c.accessKey)

	seg := newrelic.StartSegment(newrelic.FromContext(ctx), "PicsGetUserCall")

	response, err := c.client.Do(ctx, request)
	if err != nil {
		return nil, err
	}

	_ = seg.End()

	if response.Get().StatusCode != http.StatusOK {
		return nil, errors.New(fmt.Sprintf("got unsuccessful code: %d", response.Get().StatusCode))
	}

	var user resources.UserResponse
	err = json.Unmarshal(response.Get().Body, &user)
	if err != nil {
		return nil, err
	}

	return &user.Data, nil
}
