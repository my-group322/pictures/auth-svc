package data

import "context"

type IdentitiesQ interface {
	Create(accountID string) error
	GetByAccountID(accountID string) (*Identity, error)
	UpdateLastChangePassword(accountID string) error
	Verify(accountID string) error
}

type IdentitiesQCloner interface {
	Clone(ctx context.Context) IdentitiesQ
}

type Identity struct {
	IsVerified         bool
	LastPasswordChange *int64
}
