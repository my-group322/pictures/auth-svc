-- +migrate Up

create table accounts (
    id uuid primary key,
    email varchar(320) unique not null,
    password varchar(1000) not null
);

create table recovery_tokens (
    id bigserial primary key,
    email varchar(320) default '' references accounts(email) on update set default on delete cascade,
    token_hash bytea not null,
    created_at timestamp not null
);

create table verification_tokens (
    id bigserial primary key,
    email varchar(320) not null references accounts(email),
    token uuid not null
);

-- +migrate Down

drop table accounts cascade;
drop table recovery_tokens cascade;
drop table verification_tokens cascade;