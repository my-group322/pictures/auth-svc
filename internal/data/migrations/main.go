package migrations

import (
	"embed"
	"io/fs"
	"net/http"

	"github.com/pkg/errors"
	migrate "github.com/rubenv/sql-migrate"
	"github.com/sirupsen/logrus"
	"gorm.io/gorm"
)

//go:embed sql
var migrations embed.FS

func Migrate(log *logrus.Logger, db *gorm.DB, direction migrate.MigrationDirection) error {
	migrationsSubdir, err := fs.Sub(migrations, "sql")
	if err != nil {
		return errors.Wrap(err, "failed to load migrations")
	}

	migrations := &migrate.HttpFileSystemMigrationSource{FileSystem: http.FS(migrationsSubdir)}

	rawDB, _ := db.DB()
	applied, err := migrate.Exec(rawDB, "postgres", migrations, direction)
	if err != nil {
		return errors.Wrap(err, "failed to apply migrations")
	}

	directionString := "up"
	if direction == migrate.Down {
		directionString = "down"
	}

	log.WithField("applied", applied).Infof("migrations %s applied", directionString)
	return nil
}
