package data

import "context"

type SessionsQ interface {
	Create() (int64, error)
	Delete(sessionID int64) error
	IsPresent(sessionID int64) (bool, error)
}

type SessionsQCloner interface {
	Clone(ctx context.Context) SessionsQ
}
