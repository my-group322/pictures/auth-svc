package postgres

import (
	"context"

	nrcontext "github.com/ilyasiv2003/newrelic-context"

	"gitlab.com/my-group322/pictures/auth-svc/pkg/pkgdata"

	newrelic "github.com/newrelic/go-agent"
	"gitlab.com/my-group322/pictures/auth-svc/internal/data"
	"gorm.io/gorm"
)

type accountsQ struct {
	db *gorm.DB
}

func newAccountsQ(db *gorm.DB) *accountsQ {
	return &accountsQ{
		db: db,
	}
}

func NewAccountsQCloner(db *gorm.DB) data.AccountsQCloner {
	return newAccountsQ(db)
}

func (q *accountsQ) Clone(ctx context.Context) data.AccountsQ {
	return newAccountsQ(nrcontext.SetTxnToGorm(ctx, q.db))
}

func (q *accountsQ) Transaction(ctx context.Context, fn func(tx data.AccountsQ) error) error {
	seg := newrelic.StartSegment(newrelic.FromContext(ctx), "TransactionPostgres")
	defer func(seg *newrelic.Segment) {
		_ = seg.End()
	}(seg)

	return q.db.Transaction(func(tx *gorm.DB) error {
		return fn(newAccountsQ(tx))
	})
}

func (q *accountsQ) Create(ctx context.Context, account *data.Account) (string, error) {
	seg := newrelic.StartSegment(newrelic.FromContext(ctx), "CreateAccountPostgres")
	defer func(seg *newrelic.Segment) {
		_ = seg.End()
	}(seg)

	if err := q.db.Create(&account).Error; err != nil {
		return "", pkgdata.FromGormError(err)
	}

	return account.ID, nil
}

func (q *accountsQ) GetByEmail(ctx context.Context, email string) (acc *data.Account, err error) {
	seg := newrelic.StartSegment(newrelic.FromContext(ctx), "GetAccountByEmailPostgres")
	defer func(seg *newrelic.Segment) {
		_ = seg.End()
	}(seg)

	if err := q.db.Take(&acc, "email = ?", email).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, nil
		}
		return nil, err
	}
	return
}

func (q *accountsQ) GetByID(id string) (acc *data.Account, err error) {
	if err := q.db.Take(&acc, "id = ?", id).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, nil
		}
		return nil, err
	}
	return
}

func (q *accountsQ) UpdatePassword(email, password string) error {
	return q.db.
		Model(data.Account{}).
		Where("email = ?", email).
		Updates(data.Account{Password: password}).Error
}

func (q *accountsQ) DeleteRecoveryToken(token *data.RecoveryToken) error {
	return q.db.Delete(token).Error
}

func (q *accountsQ) DeleteVerificationToken(token *data.VerificationToken) error {
	return q.db.Delete(token).Error
}

func (q *accountsQ) CreateVerificationToken(token *data.VerificationToken) error {
	return q.db.Create(&token).Error
}
