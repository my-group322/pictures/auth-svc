package postgres

import (
	"context"

	nrcontext "github.com/ilyasiv2003/newrelic-context"
	"gitlab.com/my-group322/pictures/auth-svc/internal/data"
	"gorm.io/gorm"
)

type verificationTokensQ struct {
	db *gorm.DB
}

func newVerificationTokensQ(db *gorm.DB) *verificationTokensQ {
	return &verificationTokensQ{
		db: db,
	}
}

func NewVerificationTokensQCloner(db *gorm.DB) data.VerificationTokensQCloner {
	return newVerificationTokensQ(db)
}

func (q *verificationTokensQ) Clone(ctx context.Context) data.VerificationTokensQ {
	return newVerificationTokensQ(nrcontext.SetTxnToGorm(ctx, q.db))
}

func (q verificationTokensQ) Create(token *data.VerificationToken) error {
	return q.db.Create(&token).Error
}

func (q verificationTokensQ) GetByToken(token string) (data *data.VerificationToken, err error) {
	if err := q.db.Take(&data, "token = ?", token).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, nil
		}
		return nil, err
	}

	return
}

func (q verificationTokensQ) DeleteByEmail(email string) error {
	return q.db.Where("email = ?", email).Delete(&data.VerificationToken{}).Error
}
