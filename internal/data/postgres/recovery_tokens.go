package postgres

import (
	"context"
	"time"

	nrcontext "github.com/ilyasiv2003/newrelic-context"

	"gitlab.com/my-group322/pictures/auth-svc/internal/data"
	"gorm.io/gorm"
)

const tokenLifeSpan = time.Hour

type recoveryTokensQ struct {
	db *gorm.DB
}

func newRecoveryTokensQ(db *gorm.DB) *recoveryTokensQ {
	return &recoveryTokensQ{
		db: db,
	}
}

func NewRecoveryTokensQCloner(db *gorm.DB) data.RecoveryTokensQCloner {
	return newRecoveryTokensQ(db)
}

func (q *recoveryTokensQ) Clone(ctx context.Context) data.RecoveryTokensQ {
	return newRecoveryTokensQ(nrcontext.SetTxnToGorm(ctx, q.db))
}

func (q recoveryTokensQ) Create(token *data.RecoveryToken) error {
	return q.db.Create(&token).Error
}

func (q recoveryTokensQ) GetByHash(hash []byte) (token *data.RecoveryToken, err error) {
	if err := q.db.Where("token_hash = ? AND created_at > ?", hash, time.Now().Add(-tokenLifeSpan)).Take(&token).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, nil
		}
		return nil, err
	}

	return
}

func (q recoveryTokensQ) DeleteExpiredTokens() error {
	return q.db.Where("created_at < ?", time.Now().Add(-tokenLifeSpan)).Delete(&data.RecoveryToken{}).Error
}
