package data

import "context"

//go:generate mockery --case underscore --name "AccountsQ|AccountsQCloner" --outpkg datamock

type AccountsQ interface {
	Transaction(ctx context.Context, fn func(tx AccountsQ) error) error
	Create(ctx context.Context, account *Account) (string, error)
	GetByEmail(ctx context.Context, email string) (acc *Account, err error)
	GetByID(id string) (acc *Account, err error)

	UpdatePassword(email, password string) error

	// For transactions
	DeleteRecoveryToken(token *RecoveryToken) error
	DeleteVerificationToken(token *VerificationToken) error
	CreateVerificationToken(token *VerificationToken) error
}

type AccountsQCloner interface {
	Clone(ctx context.Context) AccountsQ
}

type Account struct {
	ID       string
	Email    string
	Password string
}
