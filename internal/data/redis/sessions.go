package redis

import (
	"context"

	nrcontext "github.com/ilyasiv2003/newrelic-context"

	"gitlab.com/my-group322/pictures/auth-svc/pkg/constants"

	"github.com/go-redis/redis/v8"
	"github.com/pkg/errors"
	"gitlab.com/my-group322/pictures/auth-svc/internal/data"
)

type sessionsQ struct {
	*redis.Client
}

func NewSessionsQ(cli *redis.Client) data.SessionsQCloner {
	return sessionsQ{cli}
}

func (q sessionsQ) Create() (int64, error) {
	newSessID, err := q.Incr(context.Background(), constants.RedisLastActiveSession).Result()
	if err != nil {
		return 0, errors.Wrap(err, "failed to increment sessionID")
	}

	err = q.SAdd(context.Background(), constants.RedisSessionsKey, newSessID).Err()
	if err != nil {
		return 0, errors.Wrap(err, "failed to create session")
	}

	return newSessID, nil
}

func (q sessionsQ) Delete(sessionID int64) error {
	return q.SRem(context.Background(), constants.RedisSessionsKey, sessionID).Err()
}

func (q sessionsQ) IsPresent(sessionID int64) (bool, error) {
	return q.SIsMember(context.Background(), constants.RedisSessionsKey, sessionID).Result()
}

func (q sessionsQ) Clone(ctx context.Context) data.SessionsQ {
	return sessionsQ{Client: nrcontext.WrapRedisClient(ctx, q.Client)}
}
