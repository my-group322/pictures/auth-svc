package redis

import (
	"context"
	"fmt"
	"strconv"
	"time"

	nrcontext "github.com/ilyasiv2003/newrelic-context"

	"github.com/go-redis/redis/v8"
	"gitlab.com/my-group322/pictures/auth-svc/internal/data"
	"gitlab.com/my-group322/pictures/auth-svc/pkg/constants"
)

type identitiesQ struct {
	*redis.Client
}

func NewIdentitiesQ(cli *redis.Client) data.IdentitiesQCloner {
	return identitiesQ{cli}
}

func (q identitiesQ) Create(accountID string) error {
	return q.HSet(context.TODO(), q.identityRedisKey(accountID), constants.RedisLastPassChangeKey, "", constants.RedisIsVerifiedKey, "0").Err()
}

func (q identitiesQ) GetByAccountID(accountID string) (*data.Identity, error) {
	res, err := q.HGetAll(context.TODO(), q.identityRedisKey(accountID)).Result()
	if err != nil {
		return nil, err
	}

	if len(res) == 0 {
		return nil, nil
	}

	isVerified, err := strconv.ParseBool(res[constants.RedisIsVerifiedKey])
	if err != nil {
		return nil, err
	}

	lastPassChange, err := q.parseLastPassChange(res[constants.RedisLastPassChangeKey])
	if err != nil {
		return nil, err
	}

	return &data.Identity{
		IsVerified:         isVerified,
		LastPasswordChange: lastPassChange,
	}, nil
}

func (q identitiesQ) UpdateLastChangePassword(accountID string) error {
	return q.HSet(context.TODO(), q.identityRedisKey(accountID), constants.RedisLastPassChangeKey, time.Now().Unix()).Err()
}

func (q identitiesQ) Verify(accountID string) error {
	return q.HSet(context.TODO(), q.identityRedisKey(accountID), constants.RedisIsVerifiedKey, "1").Err()
}

func (q identitiesQ) identityRedisKey(accountID string) string {
	return fmt.Sprintf("%s:%s", constants.RedisUsersKey, accountID)
}

func (q identitiesQ) parseLastPassChange(raw string) (*int64, error) {
	if raw == "" {
		return nil, nil
	}

	lastPassChange, err := strconv.ParseInt(raw, 10, 64)
	if err != nil {
		return nil, err
	}

	return &lastPassChange, nil
}

func (q identitiesQ) Clone(ctx context.Context) data.IdentitiesQ {
	return identitiesQ{Client: nrcontext.WrapRedisClient(ctx, q.Client)}
}
