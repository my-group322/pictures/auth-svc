package data

import (
	"context"
	"time"
)

//go:generate mockery --case underscore --name "RecoveryTokensQ|RecoveryTokensQCloner" --outpkg datamock

type RecoveryTokensQ interface {
	Create(token *RecoveryToken) error
	GetByHash(hash []byte) (token *RecoveryToken, err error)

	DeleteExpiredTokens() error
}

type RecoveryTokensQCloner interface {
	Clone(ctx context.Context) RecoveryTokensQ
}

type RecoveryToken struct {
	ID        int64
	Email     string
	TokenHash []byte
	CreatedAt time.Time
}
