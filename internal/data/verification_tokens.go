package data

import "context"

//go:generate mockery --case underscore --name "VerificationTokensQ|VerificationTokensQCloner" --outpkg datamock

type VerificationTokensQ interface {
	Create(token *VerificationToken) error
	GetByToken(token string) (data *VerificationToken, err error)

	DeleteByEmail(email string) error
}

type VerificationTokensQCloner interface {
	Clone(ctx context.Context) VerificationTokensQ
}

type VerificationToken struct {
	ID    int64
	Email string
	Token string
}
