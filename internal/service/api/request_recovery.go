package api

import (
	"context"
	"crypto/sha256"
	"fmt"

	"github.com/google/uuid"

	"gitlab.com/my-group322/pictures/auth-svc/internal/data"
)

func (a api) RequestRecovery(ctx context.Context, accountID string) error {
	log := a.Log.WithField("accountID", accountID)

	accountsQ := a.AccountsQ.Clone(ctx)
	recoveryTokensQ := a.RecoveryTokensQ.Clone(ctx)

	account, err := accountsQ.GetByID(accountID)
	if err != nil {
		log.WithError(err).Error("failed to get account")
		return err
	}

	if account == nil {
		return ErrAccountNotFound
	}

	token := uuid.NewString()
	tokenHash := sha256.Sum256([]byte(token))

	err = recoveryTokensQ.Create(&data.RecoveryToken{
		Email:     account.Email,
		TokenHash: tokenHash[:],
	})
	if err != nil {
		log.WithError(err).Error("failed to create token")
		return err
	}

	fmt.Println(token)
	return nil
}
