package api

import (
	"context"
	"fmt"

	"github.com/google/uuid"

	"gitlab.com/my-group322/pictures/auth-svc/internal/data"
)

func (a api) RequestVerification(ctx context.Context, accountID string) error {
	log := a.Log.WithField("accountID", accountID)

	accountsQ := a.AccountsQ.Clone(ctx)
	verifTokensQ := a.VerificationTokensQ.Clone(ctx)

	account, err := accountsQ.GetByID(accountID)
	if err != nil {
		log.WithError(err).Error("failed to get account")
		return err
	}

	if account == nil {
		return ErrAccountNotFound
	}

	identity, err := a.IdentitiesQ.Clone(ctx).GetByAccountID(accountID)
	if err != nil {
		log.WithError(err).Error("failed to get account")
		return err
	}

	if identity == nil {
		return ErrIdentityNotFound
	}

	if identity.IsVerified {
		return nil
	}

	// Cleaning up old tokens
	// Don't really want to wrap these 2 operations in tx
	if err = verifTokensQ.DeleteByEmail(account.Email); err != nil {
		log.WithError(err).Error("failed to delete tokens")
		return err
	}

	// I guess unlike the token for pass recovery, we do not need to hash it
	token := uuid.NewString()

	if err = verifTokensQ.Create(&data.VerificationToken{
		Email: account.Email,
		Token: token,
	}); err != nil {
		log.WithError(err).Error("failed to create token")
		return err
	}

	link := fmt.Sprintf("%s/verification/%s", a.Cfg.WebClientUrl, token)

	if !a.Cfg.NotificationsDisabled {
		a.Notifications.SendVerificationLink(account.Email, "Our user", link)
	}

	return nil
}
