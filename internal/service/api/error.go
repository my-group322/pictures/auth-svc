package api

import (
	"net/http"

	"gitlab.com/my-group322/pictures/auth-svc/pkg/renderer"
)

var (
	ErrAccountNotFound    = renderer.Error{Message: "picture not found", StatusCode: http.StatusNotFound}
	ErrIdentityNotFound   = renderer.Error{Message: "picture not found", StatusCode: http.StatusNotFound}
	ErrTokenNotFound      = renderer.Error{Message: "token not found", StatusCode: http.StatusNotFound}
	ErrAccountNotVerified = renderer.Error{Message: "account not verified", StatusCode: http.StatusForbidden}
	ErrAccountExists      = renderer.Error{Message: "account already exists", StatusCode: http.StatusConflict}
	ErrInvalidPassword    = renderer.Error{Message: "invalid password", StatusCode: http.StatusForbidden}
)
