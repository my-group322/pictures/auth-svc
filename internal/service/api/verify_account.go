package api

import (
	"context"

	"gitlab.com/my-group322/pictures/auth-svc/internal/data"
)

func (a api) VerifyAccount(ctx context.Context, providedToken string) error {
	log := a.Log.WithField("token", providedToken)

	accountsQ := a.AccountsQ.Clone(ctx)
	verifTokensQ := a.VerificationTokensQ.Clone(ctx)

	token, err := verifTokensQ.GetByToken(providedToken)
	if err != nil {
		log.WithError(err).Error("failed to get token")
		return err
	}

	if token == nil {
		return ErrTokenNotFound
	}

	account, err := a.AccountsQ.Clone(ctx).GetByEmail(context.TODO(), token.Email)
	if err != nil {
		log.WithError(err).Error("failed to get account")
		return err
	}

	if account == nil {
		return ErrAccountNotFound
	}

	err = accountsQ.Transaction(context.TODO(), func(tx data.AccountsQ) error {
		if err = tx.DeleteVerificationToken(token); err != nil {
			log.WithError(err).Error("failed to delete token")
			return err
		}

		if err = a.IdentitiesQ.Clone(ctx).Verify(account.ID); err != nil {
			log.WithError(err).Error("failed to verify identity")
			return err
		}

		return nil
	})
	if err != nil {
		log.WithError(err).Error("transaction failed")
		return err
	}

	return nil
}
