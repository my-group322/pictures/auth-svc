package api

import (
	"context"
)

func (a api) Logout(ctx context.Context, sessionID int64) error {
	log := a.Log.WithField("sessionID", sessionID)

	if err := a.SessionsQ.Clone(ctx).Delete(sessionID); err != nil {
		log.WithError(err).Error("failed to delete session")
		return err
	}

	return nil
}
