package api

import (
	"context"
	"fmt"
	"time"

	jares "gitlab.com/my-group322/json-api-shared"

	"gitlab.com/my-group322/pictures/auth-svc/internal/service/http/requests"

	"gitlab.com/my-group322/pictures/auth-svc/pkg/pkgdata"

	newrelic "github.com/newrelic/go-agent"

	"gitlab.com/my-group322/pictures/pics-svc/resources"

	"github.com/google/uuid"

	"github.com/dgrijalva/jwt-go"
	"github.com/pkg/errors"
	"gitlab.com/my-group322/pictures/auth-svc/internal/data"
	jwt2 "gitlab.com/my-group322/pictures/auth-svc/pkg/jwt"
	"golang.org/x/crypto/bcrypt"
)

func (a api) CreateUser(ctx context.Context, request requests.CreateUser) (string, string, error) {
	email := request.Data.Attributes.Email

	log := a.Log.WithField("email", email)

	accountsQ := a.AccountsQ.Clone(ctx)

	seg := newrelic.StartSegment(newrelic.FromContext(ctx), "BcryptPasswordGenerate")
	encryptedPassword, err := bcrypt.GenerateFromPassword(
		[]byte(request.Data.Attributes.Password),
		bcrypt.DefaultCost,
	)
	_ = seg.End()
	if err != nil {
		log.WithError(err).Error("failed to encrypt password")
		return "", "", err
	}

	account := &data.Account{
		ID:       uuid.NewString(),
		Email:    email,
		Password: string(encryptedPassword),
	}

	var (
		token     string
		accountID string
	)
	err = accountsQ.Transaction(ctx, func(tx data.AccountsQ) error {
		accountID, err = tx.Create(ctx, account)
		if err != nil {
			log.WithError(err).Error("failed to create account")

			if errors.Is(err, pkgdata.ErrUniqueViolationError) {
				return ErrAccountExists
			}

			return err
		}

		// I guess unlike the token for pass recovery, we do not need to hash it
		verificationToken := uuid.NewString()

		if err = tx.CreateVerificationToken(&data.VerificationToken{
			Email: email,
			Token: verificationToken,
		}); err != nil {
			log.WithError(err).Error("failed to create token")
			return err
		}

		if err = a.IdentitiesQ.Clone(ctx).Create(accountID); err != nil {
			log.WithError(err).Error("failed to create identity")
			return err
		}

		token, err = CreateNewTokenAndSession(a.SessionsQ.Clone(ctx), accountID, a.Cfg.JwtSecret)
		if err != nil {
			return err
		}

		if err = a.PicsSvc.CreateUser(ctx, &resources.UserResponse{
			Data: resources.User{
				Key: jares.Key{ID: accountID, Type: resources.ResourceTypeUser},
				Attributes: resources.UserAttributes{
					Email:     email,
					FirstName: request.Data.Attributes.FirstName,
					LastName:  request.Data.Attributes.LastName,
				},
			},
		}); err != nil {
			log.WithError(err).Error("failed to create user")
			return err
		}

		link := fmt.Sprintf("%s/verification/%s", a.Cfg.WebClientUrl, verificationToken)

		if !a.Cfg.NotificationsDisabled {
			a.Notifications.SendVerificationLink(email, request.Data.Attributes.FirstName, link)
		}

		return nil
	})
	if err != nil {
		a.Log.Error(err)
		return "", "", err
	}

	return token, accountID, nil
}

func CreateNewTokenAndSession(sessionsQ data.SessionsQ, userID string, jwtSecret string) (string, error) {
	newSessID, err := sessionsQ.Create()
	if err != nil {
		return "", err
	}

	claims := &jwt2.CustomClaimsJwt{
		UserID:         userID,
		SessionID:      newSessID,
		StandardClaims: jwt.StandardClaims{IssuedAt: time.Now().Unix()},
	}
	token, err := jwt.NewWithClaims(jwt.SigningMethodHS256, claims).SignedString([]byte(jwtSecret))
	if err != nil {
		return "", errors.Wrap(err, "failed to create token")
	}

	return token, nil
}
