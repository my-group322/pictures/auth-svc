package api

import (
	"context"

	jares "gitlab.com/my-group322/json-api-shared"

	"gitlab.com/my-group322/pictures/auth-svc/internal/service/http/requests"

	"gitlab.com/my-group322/pictures/auth-svc/resources"

	"golang.org/x/crypto/bcrypt"
)

func (a api) Login(ctx context.Context, request requests.Login) (*resources.AccountResponse, string, error) {
	email := request.Data.Attributes.Email

	log := a.Log.WithField("email", email)

	accountsQ := a.AccountsQ.Clone(ctx)

	account, err := accountsQ.GetByEmail(context.TODO(), email)
	if err != nil {
		log.WithError(err).Error("failed to get account")
		return nil, "", err
	}

	if account == nil {
		return nil, "", ErrAccountNotFound
	}

	if err = bcrypt.CompareHashAndPassword([]byte(account.Password), []byte(request.Data.Attributes.Password)); err != nil {
		return nil, "", ErrInvalidPassword
	}

	user, err := a.PicsSvc.GetUser(ctx, account.ID)
	if err != nil {
		log.WithError(err).Error("failed to get user")
		return nil, "", err
	}

	identity, err := a.IdentitiesQ.Clone(ctx).GetByAccountID(account.ID)
	if err != nil {
		log.WithError(err).Error("failed to get identity")
		return nil, "", err
	}

	if identity == nil {
		return nil, "", ErrIdentityNotFound
	}

	response := &resources.AccountResponse{
		Data: resources.Account{
			Key: jares.Key{ID: account.ID, Type: resources.ResourceTypeAccount},
			Attributes: resources.AccountAttributes{
				IsVerified:         identity.IsVerified,
				LastPasswordChange: identity.LastPasswordChange,
			},
		},
	}
	response.Data.Relationships.User = *user.GetKey().AsRelation()
	response.Included.Add(user)

	token, err := CreateNewTokenAndSession(a.SessionsQ.Clone(ctx), account.ID, a.Cfg.JwtSecret)
	if err != nil {
		log.Error(err)
		return nil, "", err
	}

	return response, token, nil
}
