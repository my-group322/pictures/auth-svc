package api

import (
	"context"

	"gitlab.com/my-group322/pictures/auth-svc/internal/service/http/requests"

	"gitlab.com/my-group322/pictures/auth-svc/internal/service/app"
	"gitlab.com/my-group322/pictures/auth-svc/resources"
)

//go:generate mockery --case underscore --name API --outpkg apimock

type API interface {
	CreateUser(ctx context.Context, request requests.CreateUser) (string, string, error)
	Login(ctx context.Context, request requests.Login) (*resources.AccountResponse, string, error)
	Logout(ctx context.Context, sessionID int64) error
	RecoverPassword(ctx context.Context, request requests.RecoverPassword, providedToken string) error
	RequestRecovery(ctx context.Context, accountID string) error
	RequestVerification(ctx context.Context, accountID string) error
	VerifyAccount(ctx context.Context, providedToken string) error
}

type api struct {
	*app.App
}

func New(app *app.App) API {
	return &api{app}
}
