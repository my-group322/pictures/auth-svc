package api

import (
	"context"
	"crypto/sha256"

	"gitlab.com/my-group322/pictures/auth-svc/internal/service/http/requests"

	"gitlab.com/my-group322/pictures/auth-svc/internal/data"
	"golang.org/x/crypto/bcrypt"
)

func (a api) RecoverPassword(ctx context.Context, request requests.RecoverPassword, providedToken string) error {
	log := a.Log.WithField("recovery_token", providedToken)

	accountsQ := a.AccountsQ.Clone(ctx)
	recoveryTokensQ := a.RecoveryTokensQ.Clone(ctx)

	tokenHash := sha256.Sum256([]byte(providedToken))

	token, err := recoveryTokensQ.GetByHash(tokenHash[:])
	if err != nil {
		log.WithError(err).Error("failed to get token")
		return err
	}

	if token == nil {
		return ErrTokenNotFound
	}

	encryptedPassword, err := bcrypt.GenerateFromPassword(
		[]byte(request.Data.Attributes.NewPassword),
		bcrypt.DefaultCost,
	)
	if err != nil {
		log.WithError(err).Error("failed to encrypt password")
		return err
	}

	account, err := a.AccountsQ.Clone(ctx).GetByEmail(context.TODO(), token.Email)
	if err != nil {
		log.WithError(err).Error("failed to get account")
		return err
	}

	err = accountsQ.Transaction(context.TODO(), func(tx data.AccountsQ) error {
		if err = tx.UpdatePassword(token.Email, string(encryptedPassword)); err != nil {
			log.WithError(err).Error("failed to update password")
			return err
		}

		if err = tx.DeleteRecoveryToken(token); err != nil {
			log.WithError(err).Error("failed to delete token")
			return err
		}

		// Invalidates all active sessions
		if err = a.IdentitiesQ.Clone(ctx).UpdateLastChangePassword(account.ID); err != nil {
			log.WithError(err).Error("failed to update last change password")
			return err
		}

		return nil
	})
	if err != nil {
		log.WithError(err).Error("transaction failed")
		return err
	}

	return nil
}
