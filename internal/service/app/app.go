package app

import (
	"strings"

	redislib "github.com/go-redis/redis/v8"
	client "github.com/ilyasiv2003/go-http-client"
	"github.com/ilyasiv2003/newrelic-context/nrgorm"
	newrelic "github.com/newrelic/go-agent"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"gitlab.com/my-group322/pictures/auth-svc/internal/config"
	"gitlab.com/my-group322/pictures/auth-svc/internal/connectors"
	"gitlab.com/my-group322/pictures/auth-svc/internal/data"
	"gitlab.com/my-group322/pictures/auth-svc/internal/data/postgres"
	"gitlab.com/my-group322/pictures/auth-svc/internal/data/redis"
	"gitlab.com/my-group322/pictures/auth-svc/internal/rabbitmq"
	driver "gorm.io/driver/postgres"
	"gorm.io/gorm"
)

// App holds all main dependencies that have to be initialized on start
// It is defined here but not in app package to break import cycle
type App struct {
	Log                 *logrus.Logger
	Cfg                 *config.Config
	DB                  *gorm.DB
	PicsSvc             connectors.Connector
	Notifications       rabbitmq.Publisher
	SessionsQ           data.SessionsQCloner
	IdentitiesQ         data.IdentitiesQCloner
	AccountsQ           data.AccountsQCloner
	RecoveryTokensQ     data.RecoveryTokensQCloner
	VerificationTokensQ data.VerificationTokensQCloner
	Metrics             newrelic.Application
}

func New(cfg *config.Config) *App {
	var err error

	a := &App{
		Log: logrus.New(),
		Cfg: cfg,
	}

	a.DB, err = gorm.Open(driver.Open(cfg.DBUrl), &gorm.Config{SkipDefaultTransaction: true})
	if err != nil {
		panic(errors.Wrap(err, "failed to open database"))
	}
	nrgorm.AddGormCallbacks(a.DB)

	a.AccountsQ = postgres.NewAccountsQCloner(a.DB)
	a.RecoveryTokensQ = postgres.NewRecoveryTokensQCloner(a.DB)
	a.VerificationTokensQ = postgres.NewVerificationTokensQCloner(a.DB)

	a.PicsSvc = connectors.NewConnector(client.New(cfg.PicsSvcUrl), cfg.InternalAccessKey)

	a.Notifications = rabbitmq.NewPublisher(a.Log, cfg.RabbitMQUrl)

	redisCli := redislib.NewClient(&redislib.Options{
		Addr:     cfg.RedisUrl,
		Password: "",
		DB:       0,
	})

	a.SessionsQ = redis.NewSessionsQ(redisCli)
	a.IdentitiesQ = redis.NewIdentitiesQ(redisCli)

	if a.Cfg.NewRelicLicenceKey == "" {
		a.Log.Warn("metrics are disabled")

		// Disabling app with invalid key
		a.Cfg.NewRelicLicenceKey = strings.Repeat("a", 40)
	}
	a.Metrics, err = newrelic.NewApplication(newrelic.NewConfig("auth-svc", a.Cfg.NewRelicLicenceKey))
	if err != nil {
		panic(errors.Wrap(err, "failed to setup metrics"))
	}

	return a
}
