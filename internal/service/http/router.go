package http

import (
	"gitlab.com/my-group322/pictures/auth-svc/internal/service/app"

	"gitlab.com/my-group322/pictures/auth-svc/internal/service/http/middlewares"
	"gitlab.com/my-group322/pictures/auth-svc/internal/service/http/server"

	"github.com/go-chi/chi/v5/middleware"
	nrcontext "github.com/ilyasiv2003/newrelic-context"

	"github.com/go-chi/chi/v5"
)

func Router(a *app.App, server *server.Server) chi.Router {
	r := chi.NewRouter()

	r.Use(
		middleware.Heartbeat("/ping"),
		nrcontext.NewMiddlewareWithApp(a.Metrics).Handler,
		middleware.Recoverer,
	)

	r.Route("/accounts/{id}", func(r chi.Router) {
		r.Post("/recovery", server.RequestRecovery)

		// For re-sending verification emails
		r.Post("/verification", server.RequestVerification)
	})

	r.Post("/registration", server.CreateUser)
	r.Post("/login", server.Login)

	r.With(middlewares.AuthenticationMiddleware(a)).
		Post("/logout", server.Logout)

	r.Post("/verification/{token}", server.VerifyAccount)
	r.Post("/recovery/{token}", server.RecoverPassword)

	return r
}
