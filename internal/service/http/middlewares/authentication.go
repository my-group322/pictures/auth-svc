package middlewares

import (
	"net/http"

	ctx2 "gitlab.com/my-group322/pictures/auth-svc/internal/service/ctx"

	"gitlab.com/my-group322/pictures/auth-svc/pkg/renderer"

	"github.com/dgrijalva/jwt-go"
	"gitlab.com/my-group322/pictures/auth-svc/internal/service/app"
	jwt2 "gitlab.com/my-group322/pictures/auth-svc/pkg/jwt"
)

func AuthenticationMiddleware(app *app.App) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			authCookie, err := r.Cookie("jwt-auth")
			if err != nil {
				app.Log.WithError(err).Error("cookie wasn't found")
				renderer.RenderStatus(w, http.StatusUnauthorized)
				return
			}

			token, err := jwt.ParseWithClaims(authCookie.Value, &jwt2.CustomClaimsJwt{}, func(t *jwt.Token) (interface{}, error) {
				return []byte(app.Cfg.JwtSecret), nil
			})
			if err != nil {
				app.Log.WithError(err).Error("token didn't pass verification")
				renderer.RenderStatus(w, http.StatusUnauthorized)
				return
			}

			claims := token.Claims.(*jwt2.CustomClaimsJwt)

			ctx := r.Context()
			ctx = ctx2.CtxRequester(claims.UserID)(ctx)
			ctx = ctx2.CtxSessionID(claims.SessionID)(ctx)

			sessionsQ := app.SessionsQ.Clone(ctx)

			sessionValid, err := sessionsQ.IsPresent(claims.SessionID)
			if err != nil {
				app.Log.WithError(err).Error("failed to check session id")
				renderer.RenderStatus(w, http.StatusInternalServerError)
				return
			}

			if !sessionValid {
				app.Log.Error("session not valid")
				renderer.RenderStatus(w, http.StatusUnauthorized)
				return
			}

			identity, err := app.IdentitiesQ.Clone(ctx).GetByAccountID(claims.UserID)
			if err != nil {
				app.Log.WithError(err).Error("failed to get requester identity")
				renderer.RenderStatus(w, http.StatusInternalServerError)
				return
			}

			if identity == nil {
				app.Log.WithError(err).Error("identity not found")
				renderer.RenderStatus(w, http.StatusInternalServerError)
				return
			}

			// Session was created after pass change - we should expire it
			if identity.LastPasswordChange != nil && *identity.LastPasswordChange > claims.IssuedAt {
				err := sessionsQ.Delete(claims.SessionID)
				if err != nil {
					app.Log.WithError(err).Error("failed to delete session")
					renderer.RenderStatus(w, http.StatusInternalServerError)
					return
				}

				renderer.RenderStatus(w, http.StatusUnauthorized)
				return
			}

			next.ServeHTTP(w, r.WithContext(ctx))
		})
	}
}
