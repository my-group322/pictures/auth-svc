package requests

import (
	"encoding/json"
	"net/http"

	"gitlab.com/my-group322/pictures/auth-svc/resources"

	validation "github.com/go-ozzo/ozzo-validation"
)

type Login struct {
	Data resources.Login `json:"data"`
}

func NewLogin(r *http.Request) (Login, error) {
	var request Login

	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		return request, validation.Errors{"request": err}
	}

	return request, request.validate()
}

func (r *Login) validate() error {
	errs := validation.Errors{
		"/data/attributes/password": validation.Validate(r.Data.Attributes.Password, validation.Required),
		"/data/attributes/email":    validation.Validate(r.Data.Attributes.Email, validation.Required),
	}

	return errs.Filter()
}
