package requests

import (
	"encoding/json"
	"net/http"

	"gitlab.com/my-group322/pictures/auth-svc/resources"

	validation "github.com/go-ozzo/ozzo-validation"
)

type CreateUser struct {
	Data resources.CreateUser `json:"data"`
}

func NewCreateUser(r *http.Request) (CreateUser, error) {
	var request CreateUser

	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		return request, validation.Errors{"request": err}
	}

	return request, request.validate()
}

func (r *CreateUser) validate() error {
	errs := validation.Errors{
		"/data/attributes/email": validation.Validate(r.Data.Attributes.Email, validation.Required),
		"/data/attributes/password": validation.Validate(r.Data.Attributes.Password,
			validation.Required,
			validation.Length(8, 0)),
		"/data/attributes/first_name": validation.Validate(r.Data.Attributes.FirstName,
			validation.Required,
			validation.Length(0, 30)),
		"/data/attributes/last_name": validation.Validate(r.Data.Attributes.LastName,
			validation.Required,
			validation.Length(0, 30)),
	}

	return errs.Filter()
}
