package requests

import (
	"encoding/json"
	"net/http"

	"gitlab.com/my-group322/pictures/auth-svc/resources"

	validation "github.com/go-ozzo/ozzo-validation"
)

type RecoverPassword struct {
	Data resources.RecoverPassword `json:"data"`
}

func NewRecoverPassword(r *http.Request) (RecoverPassword, error) {
	var request RecoverPassword

	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		return request, validation.Errors{"request": err}
	}

	return request, request.validate()
}

func (r *RecoverPassword) validate() error {
	errs := validation.Errors{
		"/data/attributes/new_password": validation.Validate(r.Data.Attributes.NewPassword,
			validation.Required,
			validation.Length(8, 0)),
	}

	return errs.Filter()
}
