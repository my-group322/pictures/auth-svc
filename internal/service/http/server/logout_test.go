package server

import (
	"context"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/my-group322/pictures/auth-svc/internal/service/api"

	ctx2 "gitlab.com/my-group322/pictures/auth-svc/internal/service/ctx"

	"gitlab.com/my-group322/pictures/auth-svc/internal/service/app"

	"gitlab.com/my-group322/pictures/auth-svc/internal/config"

	"gitlab.com/my-group322/pictures/auth-svc/pkg/constants"

	"gitlab.com/my-group322/pictures/auth-svc/internal/data/redis"

	"github.com/dgrijalva/jwt-go"
	"github.com/stretchr/testify/require"
	jwt2 "gitlab.com/my-group322/pictures/auth-svc/pkg/jwt"

	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
)

func TestServer_Logout(t *testing.T) {
	var (
		log       = logrus.New()
		jwtSecret = uuid.NewString()
	)

	a := api.New(&app.App{
		Cfg:         &config.Config{JwtSecret: jwtSecret},
		Log:         log,
		SessionsQ:   redis.NewSessionsQ(redisCli),
		IdentitiesQ: redis.NewIdentitiesQ(redisCli),
	})
	server := New(a, log)

	tokenRaw, err := api.CreateNewTokenAndSession(redis.NewSessionsQ(redisCli).Clone(context.Background()), uuid.NewString(), jwtSecret)
	require.NoError(t, err)

	token, err := jwt.ParseWithClaims(tokenRaw, &jwt2.CustomClaimsJwt{}, func(t *jwt.Token) (interface{}, error) {
		return []byte(jwtSecret), nil
	})
	require.NoError(t, err)

	claims := token.Claims.(*jwt2.CustomClaimsJwt)

	ok, err := redisCli.SIsMember(context.TODO(), constants.RedisSessionsKey, claims.SessionID).Result()
	require.NoError(t, err)
	require.True(t, ok)

	request, _ := http.NewRequest("", "", nil)
	response := httptest.NewRecorder()

	ctx := ctx2.CtxSessionID(claims.SessionID)(request.Context())

	handler := http.HandlerFunc(server.Logout)
	handler.ServeHTTP(response, request.WithContext(ctx))

	require.Equal(t, http.StatusUnauthorized, response.Code)
	require.Equal(t, "jwt-auth=; HttpOnly; SameSite=Strict", response.Header().Get("Set-Cookie"))

	ok, err = redisCli.SIsMember(context.TODO(), constants.RedisSessionsKey, claims.SessionID).Result()
	require.NoError(t, err)
	require.False(t, ok)
}
