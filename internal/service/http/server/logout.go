package server

import (
	"net/http"

	"gitlab.com/my-group322/pictures/auth-svc/internal/service/ctx"

	"gitlab.com/my-group322/pictures/auth-svc/pkg/renderer"
)

func (s Server) Logout(w http.ResponseWriter, r *http.Request) {
	if err := s.api.Logout(r.Context(), ctx.SessionID(r)); err != nil {
		s.log.Error(err)
		renderer.RenderErr(w, renderer.NewJsonErr(err)...)
		return
	}

	// Deleting cookie
	http.SetCookie(w, &http.Cookie{
		Name:     "jwt-auth",
		Value:    "",
		HttpOnly: true,
		SameSite: http.SameSiteStrictMode,
	})

	renderer.RenderStatus(w, http.StatusUnauthorized)
}
