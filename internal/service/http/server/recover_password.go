package server

import (
	"net/http"

	"gitlab.com/my-group322/pictures/auth-svc/internal/service/http/requests"

	"gitlab.com/my-group322/pictures/auth-svc/pkg/renderer"

	"github.com/go-chi/chi/v5"
)

func (s Server) RecoverPassword(w http.ResponseWriter, r *http.Request) {
	providedToken := chi.URLParam(r, "token")

	request, err := requests.NewRecoverPassword(r)
	if err != nil {
		renderer.RenderErr(w, renderer.BadRequestErr(err)...)
		return
	}

	if err = s.api.RecoverPassword(r.Context(), request, providedToken); err != nil {
		s.log.Error(err)
		renderer.RenderErr(w, renderer.NewJsonErr(err)...)
	}
}
