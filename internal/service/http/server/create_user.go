package server

import (
	"encoding/json"
	"net/http"

	jares "gitlab.com/my-group322/json-api-shared"

	"gitlab.com/my-group322/pictures/auth-svc/internal/service/http/requests"

	"gitlab.com/my-group322/pictures/auth-svc/pkg/renderer"

	"gitlab.com/my-group322/pictures/pics-svc/resources"
)

func (s Server) CreateUser(w http.ResponseWriter, r *http.Request) {
	request, err := requests.NewCreateUser(r)
	if err != nil {
		renderer.RenderErr(w, renderer.BadRequestErr(err)...)
		return
	}

	token, accountID, err := s.api.CreateUser(r.Context(), request)
	if err != nil {
		s.log.Error(err)
		renderer.RenderErr(w, renderer.NewJsonErr(err)...)
		return
	}

	http.SetCookie(w, &http.Cookie{
		Name:     "jwt-auth",
		Value:    token,
		Path:     "/",
		HttpOnly: true,
		SameSite: http.SameSiteDefaultMode,
	})

	response := resources.UserResponse{
		Data: resources.User{
			Key: jares.Key{ID: accountID, Type: resources.ResourceTypeUser},
		},
	}

	_ = json.NewEncoder(w).Encode(response)
}
