package server

import (
	"bytes"
	"context"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"gitlab.com/my-group322/pictures/auth-svc/internal/service/api"

	"gitlab.com/my-group322/pictures/auth-svc/internal/service/http/requests"

	"gitlab.com/my-group322/pictures/auth-svc/pkg/pkgdata"

	"gitlab.com/my-group322/pictures/auth-svc/internal/service/app"
	"gitlab.com/my-group322/pictures/auth-svc/resources"

	"gitlab.com/my-group322/pictures/auth-svc/internal/data/redis"

	"github.com/go-chi/chi/v5"
	"gitlab.com/my-group322/pictures/auth-svc/internal/config"
	"gitlab.com/my-group322/pictures/auth-svc/internal/data"

	"github.com/alicebob/miniredis/v2"
	redislib "github.com/go-redis/redis/v8"
	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	connectorsMocks "gitlab.com/my-group322/pictures/auth-svc/internal/connectors/mocks"
	datamock "gitlab.com/my-group322/pictures/auth-svc/internal/data/mocks"
)

var redisCli *redislib.Client

func TestMain(m *testing.M) {
	os.Exit(setup(m))
}

func setup(m *testing.M) int {
	miniRedis, err := miniredis.Run()
	if err != nil {
		panic(err)
	}
	defer miniRedis.Close()

	redisCli = redislib.NewClient(&redislib.Options{
		Addr: miniRedis.Addr(),
	})

	return m.Run()
}

func TestServer_CreateUser(t *testing.T) {
	var (
		ctx            = context.Background()
		log            = logrus.New()
		accountID      = uuid.NewString()
		userEmail      = uuid.NewString()
		accountsQClone = &datamock.AccountsQCloner{}
		accountsQ      = &datamock.AccountsQ{}
		picsSvc        = &connectorsMocks.Connector{}
	)

	picsSvc.On("CreateUser", ctx, mock.Anything).Return(nil)

	accountsQClone.On("Clone", mock.Anything).Return(accountsQ)

	newAPI := api.New(&app.App{
		Log:         log,
		AccountsQ:   accountsQClone,
		SessionsQ:   redis.NewSessionsQ(redisCli),
		IdentitiesQ: redis.NewIdentitiesQ(redisCli),
		PicsSvc:     picsSvc,
		Cfg:         &config.Config{NotificationsDisabled: true},
	})
	server := New(newAPI, log)

	validReq, err := json.Marshal(&requests.CreateUser{
		Data: resources.CreateUser{
			Attributes: resources.CreateUserAttributes{
				Email:     userEmail,
				Password:  "reallyNicePassword",
				FirstName: "Illia",
				LastName:  "Sivoshapka",
			},
		},
	})
	require.NoError(t, err)

	t.Run("positive", func(t *testing.T) {
		accountsQ.On("GetByEmail", ctx, userEmail).Return(nil, nil).Once()
		accountsQ.On("Transaction", ctx, mock.Anything).Run(func(args mock.Arguments) {
			txCallback, ok := args.Get(1).(func(tx data.AccountsQ) error)
			require.True(t, ok)

			err := txCallback(accountsQ)
			require.NoError(t, err)
		}).Return(nil).Once()

		accountsQ.On("Create", ctx, mock.Anything).Return(accountID, nil).Once()
		accountsQ.On("CreateVerificationToken", mock.Anything).Return(nil)

		response := recordHandler(server.CreateUser, validReq)
		require.Equal(t, http.StatusOK, response.Code)
	})

	t.Run("negative_account_exists", func(t *testing.T) {
		accountsQ.On("Create", ctx, mock.Anything).Return("", pkgdata.ErrUniqueViolationError).Once()

		accountsQ.On("Transaction", ctx, mock.Anything).Run(func(args mock.Arguments) {
			txCallback, ok := args.Get(1).(func(tx data.AccountsQ) error)
			require.True(t, ok)

			err = txCallback(accountsQ)
			require.ErrorIs(t, err, api.ErrAccountExists)
		}).Return(api.ErrAccountExists).Once()

		response := recordHandler(server.CreateUser, validReq)
		require.Equal(t, http.StatusConflict, response.Code)
	})

	t.Run("negative_short_password", func(t *testing.T) {
		req, err := json.Marshal(&requests.CreateUser{
			Data: resources.CreateUser{
				Attributes: resources.CreateUserAttributes{
					Email:     userEmail,
					Password:  "short",
					FirstName: "Illia",
					LastName:  "Sivoshapka",
				},
			},
		})
		require.NoError(t, err)

		response := recordHandler(server.CreateUser, req)
		require.Equal(t, http.StatusBadRequest, response.Code)
	})
}

func recordHandler(handlerFunc func(w http.ResponseWriter, r *http.Request), body []byte, paramsCtx ...*chi.Context) *httptest.ResponseRecorder {
	request, _ := http.NewRequest("", "", bytes.NewBuffer(body))
	response := httptest.NewRecorder()

	ctx := request.Context()
	if paramsCtx != nil {
		ctx = context.WithValue(ctx, chi.RouteCtxKey, paramsCtx[0])
	}

	handler := http.HandlerFunc(handlerFunc)
	handler.ServeHTTP(response, request.WithContext(ctx))

	return response
}
