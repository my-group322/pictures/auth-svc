package server

import (
	"context"
	"encoding/json"
	"net/http"
	"testing"

	"gitlab.com/my-group322/pictures/auth-svc/resources"

	jares "gitlab.com/my-group322/json-api-shared"

	"gitlab.com/my-group322/pictures/auth-svc/internal/service/api"

	"gitlab.com/my-group322/pictures/auth-svc/internal/service/http/requests"

	"gitlab.com/my-group322/pictures/auth-svc/internal/service/app"
	picsresources "gitlab.com/my-group322/pictures/pics-svc/resources"

	"gitlab.com/my-group322/pictures/auth-svc/pkg/constants"

	"gitlab.com/my-group322/pictures/auth-svc/internal/data/redis"

	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"gitlab.com/my-group322/pictures/auth-svc/internal/config"
	connectorsMocks "gitlab.com/my-group322/pictures/auth-svc/internal/connectors/mocks"
	"gitlab.com/my-group322/pictures/auth-svc/internal/data"
	datamock "gitlab.com/my-group322/pictures/auth-svc/internal/data/mocks"
	"golang.org/x/crypto/bcrypt"
)

func TestServer_Login(t *testing.T) {
	var (
		ctx            = context.Background()
		log            = logrus.New()
		accountID      = uuid.NewString()
		userEmail      = uuid.NewString()
		accountsQClone = &datamock.AccountsQCloner{}
		accountsQ      = &datamock.AccountsQ{}
		picsSvc        = &connectorsMocks.Connector{}
		password       = uuid.NewString()
	)

	picsSvc.On("GetUser", ctx, mock.Anything).Return(&picsresources.User{Key: jares.Key{ID: accountID, Type: picsresources.ResourceTypeUser}}, nil)

	accountsQClone.On("Clone", mock.Anything).Return(accountsQ)

	passwordHash, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	require.NoError(t, err)

	newAPI := api.New(&app.App{
		Log:         log,
		AccountsQ:   accountsQClone,
		SessionsQ:   redis.NewSessionsQ(redisCli),
		IdentitiesQ: redis.NewIdentitiesQ(redisCli),
		PicsSvc:     picsSvc,
		Cfg:         &config.Config{NotificationsDisabled: true},
	})
	server := New(newAPI, log)

	validReq, err := json.Marshal(&requests.Login{
		Data: resources.Login{
			Attributes: resources.LoginAttributes{
				Email:    userEmail,
				Password: password,
			},
		},
	})
	require.NoError(t, err)

	err = redis.NewIdentitiesQ(redisCli).Clone(context.Background()).Create(accountID)
	require.NoError(t, err)

	t.Run("positive", func(t *testing.T) {
		accountsQ.On("GetByEmail", ctx, userEmail).Return(&data.Account{ID: accountID, Password: string(passwordHash)}, nil).Once()

		response := recordHandler(server.Login, validReq)
		require.Equal(t, http.StatusOK, response.Code)
		require.NotEmpty(t, response.Header().Get("Set-Cookie"))

		res, err := redisCli.Get(context.TODO(), "active:sessions:last").Result()
		require.NoError(t, err)
		require.NotEmpty(t, res)

		ok, err := redisCli.SIsMember(context.TODO(), constants.RedisSessionsKey, res).Result()
		require.NoError(t, err)
		require.True(t, ok)
	})

	t.Run("negative_account_not_found", func(t *testing.T) {
		accountsQ.On("GetByEmail", ctx, userEmail).Return(nil, nil).Once()

		response := recordHandler(server.Login, validReq)
		require.Equal(t, http.StatusNotFound, response.Code)
	})

	t.Run("negative_wrong_password", func(t *testing.T) {
		req, err := json.Marshal(&requests.Login{
			Data: resources.Login{
				Attributes: resources.LoginAttributes{
					Email:    userEmail,
					Password: uuid.NewString(),
				},
			},
		})
		require.NoError(t, err)

		accountsQ.On("GetByEmail", ctx, userEmail).Return(&data.Account{ID: accountID, Password: string(passwordHash)}, nil).Once()

		response := recordHandler(server.Login, req)
		require.Equal(t, http.StatusForbidden, response.Code)
	})

	t.Run("negative_invalid_body", func(t *testing.T) {
		response := recordHandler(server.Login, nil)
		require.Equal(t, http.StatusBadRequest, response.Code)
	})
}
