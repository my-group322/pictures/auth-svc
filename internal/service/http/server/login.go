package server

import (
	"encoding/json"
	"net/http"

	"gitlab.com/my-group322/pictures/auth-svc/internal/service/http/requests"

	"gitlab.com/my-group322/pictures/auth-svc/pkg/renderer"
)

func (s Server) Login(w http.ResponseWriter, r *http.Request) {
	request, err := requests.NewLogin(r)
	if err != nil {
		renderer.RenderErr(w, renderer.BadRequestErr(err)...)
		return
	}

	response, token, err := s.api.Login(r.Context(), request)
	if err != nil {
		s.log.Error(err)
		renderer.RenderErr(w, renderer.NewJsonErr(err)...)
		return
	}

	http.SetCookie(w, &http.Cookie{
		Name:     "jwt-auth",
		Value:    token,
		Path:     "/",
		HttpOnly: true,
		SameSite: http.SameSiteDefaultMode,
	})

	_ = json.NewEncoder(w).Encode(response)
}
