package server

import (
	"net/http"

	"gitlab.com/my-group322/pictures/auth-svc/pkg/renderer"

	"github.com/go-chi/chi/v5"
)

func (s Server) RequestVerification(w http.ResponseWriter, r *http.Request) {
	accountID := chi.URLParam(r, "id")

	if err := s.api.RequestVerification(r.Context(), accountID); err != nil {
		s.log.Error(err)
		renderer.RenderErr(w, renderer.NewJsonErr(err)...)
	}
}
