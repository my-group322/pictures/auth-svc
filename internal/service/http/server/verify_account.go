package server

import (
	"net/http"

	"gitlab.com/my-group322/pictures/auth-svc/pkg/renderer"

	"github.com/go-chi/chi/v5"
)

func (s Server) VerifyAccount(w http.ResponseWriter, r *http.Request) {
	providedToken := chi.URLParam(r, "token")

	if err := s.api.VerifyAccount(r.Context(), providedToken); err != nil {
		s.log.Error(err)
		renderer.RenderErr(w, renderer.NewJsonErr(err)...)
	}
}
