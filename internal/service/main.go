package service

import (
	"context"
	"net/http"

	"gitlab.com/my-group322/pictures/auth-svc/internal/service/http/server"

	"gitlab.com/my-group322/pictures/auth-svc/internal/service/api"
	"gitlab.com/my-group322/pictures/auth-svc/internal/service/app"

	"github.com/pkg/errors"

	myhttp "gitlab.com/my-group322/pictures/auth-svc/internal/service/http"
)

type Service struct {
	app *app.App
}

func NewService(app *app.App) *Service {
	return &Service{app}
}

func (s *Service) Run(ctx context.Context) error {
	newApi := api.New(s.app)
	newServer := server.New(newApi, s.app.Log)

	r := myhttp.Router(s.app, newServer)

	s.runRecoveryTokensCleaner()

	listener := &http.Server{Addr: "0.0.0.0:" + s.app.Cfg.Port, Handler: r}

	errChan := make(chan error)
	go func() {
		if err := listener.ListenAndServe(); err != nil {
			errChan <- errors.Wrap(err, "router crashed")
		}
	}()

	select {
	case err := <-errChan:
		return err
	case <-ctx.Done():
		if err := listener.Shutdown(context.Background()); err != nil {
			return err
		}
		return nil
	}
}
