package ctx

import (
	"context"
	"net/http"
)

type ctxKey int

const (
	requesterCtxKey ctxKey = iota
	sessionIDCtxKey
)

func CtxRequester(userID string) func(context.Context) context.Context {
	return func(ctx context.Context) context.Context {
		return context.WithValue(ctx, requesterCtxKey, userID)
	}
}

func Requester(r *http.Request) string {
	return r.Context().Value(requesterCtxKey).(string)
}

func CtxSessionID(id int64) func(context.Context) context.Context {
	return func(ctx context.Context) context.Context {
		return context.WithValue(ctx, sessionIDCtxKey, id)
	}
}

func SessionID(r *http.Request) int64 {
	return r.Context().Value(sessionIDCtxKey).(int64)
}
