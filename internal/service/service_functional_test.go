//go:build functional
// +build functional

package service

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"

	"gitlab.com/my-group322/pictures/auth-svc/internal/service/api"
	myhttp "gitlab.com/my-group322/pictures/auth-svc/internal/service/http"

	"gitlab.com/my-group322/pictures/auth-svc/internal/service/http/requests"
	"gitlab.com/my-group322/pictures/auth-svc/internal/service/http/server"

	"gitlab.com/my-group322/pictures/auth-svc/internal/service/app"
	"gitlab.com/my-group322/pictures/auth-svc/resources"

	migrate "github.com/rubenv/sql-migrate"

	"github.com/stretchr/testify/mock"

	publishermock "gitlab.com/my-group322/pictures/auth-svc/internal/rabbitmq/mocks"

	"github.com/google/uuid"

	httpcli "github.com/ilyasiv2003/go-http-client"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/require"
	"gitlab.com/my-group322/pictures/auth-svc/internal/config"
	"gitlab.com/my-group322/pictures/auth-svc/internal/data/migrations"
)

var (
	testServer     *httptest.Server
	a              *app.App
	testHTTPClient httpcli.Client
)

func TestMain(m *testing.M) {
	os.Exit(setup(m))
}

// This func is necessary because of os.Exit and defer conflicts
func setup(m *testing.M) int {
	log := logrus.New()

	cfg, err := config.New(log)
	if err != nil {
		panic(errors.Wrap(err, "failed to setup config"))
	}

	cfg.NotificationsDisabled = false

	a = app.New(cfg)

	rawDB, _ := a.DB.DB()
	defer func() {
		if err = rawDB.Close(); err != nil {
			log.WithError(err).Fatalf("failed to close db")
		}
	}()

	if err = migrations.Migrate(log, a.DB, migrate.Up); err != nil {
		panic(errors.Wrap(err, "failed to apply migrations"))
	}

	newApi := api.New(a)

	testRouter := myhttp.Router(a, server.New(newApi, log))

	testServer = httptest.NewServer(testRouter)
	defer testServer.Close()

	testHTTPClient = httpcli.New(testServer.URL)

	return m.Run()
}

// As a user, I want to register, verify my account, log into and logout from my account.
// After logout my session should be invalid.
func TestAuthFlow(t *testing.T) {
	verifToken, email, password := register(t)
	verifyAccount(t, verifToken)

	cookie := login(t, email, password)

	token := getTokenFromCookie(cookie)
	logout(t, token)
}

func getTokenFromCookie(cookie string) string {
	jwtAuthPart := strings.Split(cookie, ";")[0]
	return strings.TrimPrefix(jwtAuthPart, "jwt-auth=")
}

func register(t *testing.T) (string, string, string) {
	var (
		email    = uuid.NewString()
		password = uuid.NewString()
	)
	createUser, err := json.Marshal(&resources.CreateUserResponse{
		Data: resources.CreateUser{
			Attributes: resources.CreateUserAttributes{
				Email:     email,
				Password:  password,
				FirstName: "Illia",
				LastName:  "Syvoshapka",
			},
		},
	})
	require.NoError(t, err)

	req, err := http.NewRequest(http.MethodPost, fmt.Sprintf("%s/%s", testServer.URL, "registration"), bytes.NewBuffer(createUser))
	require.NoError(t, err)

	notifications := publishermock.Publisher{}
	a.Notifications = &notifications

	var verifLink string
	notifications.On("SendVerificationLink", email, mock.Anything, mock.Anything).Run(func(args mock.Arguments) {
		link, ok := args.Get(2).(string)
		require.True(t, ok)

		verifLink = link
	}).Once()

	resp, err := testHTTPClient.Do(context.Background(), req)
	require.NoError(t, err)
	require.Equal(t, http.StatusOK, resp.Get().StatusCode)

	parts := strings.Split(verifLink, "/")
	require.NotEmpty(t, parts)

	return parts[len(parts)-1], email, password
}

func login(t *testing.T, email, password string) string {
	login, err := json.Marshal(requests.Login{
		Data: resources.Login{
			Attributes: resources.LoginAttributes{
				Email:    email,
				Password: password,
			},
		},
	})
	require.NoError(t, err)

	req, err := http.NewRequest(http.MethodPost, fmt.Sprintf("%s/%s", testServer.URL, "login"), bytes.NewBuffer(login))
	require.NoError(t, err)

	resp, err := testHTTPClient.Do(context.Background(), req)
	require.NoError(t, err)
	require.Equal(t, http.StatusOK, resp.Get().StatusCode)

	return resp.Get().Header.Get("Set-Cookie")
}

func verifyAccount(t *testing.T, token string) {
	req, err := http.NewRequest(http.MethodPost, fmt.Sprintf("%s/%s/%s", testServer.URL, "verification", token), nil)
	require.NoError(t, err)

	resp, err := testHTTPClient.Do(context.Background(), req)
	require.NoError(t, err)
	require.Equal(t, http.StatusOK, resp.Get().StatusCode)
}

func logout(t *testing.T, token string) {
	req, err := http.NewRequest(http.MethodPost, fmt.Sprintf("%s/%s", testServer.URL, "logout"), nil)
	require.NoError(t, err)

	req.Header.Set("Cookie", fmt.Sprintf("%s=%s", "jwt-auth", token))

	resp, err := testHTTPClient.Do(context.Background(), req)
	require.NoError(t, err)
	require.Equal(t, http.StatusUnauthorized, resp.Get().StatusCode)
}
