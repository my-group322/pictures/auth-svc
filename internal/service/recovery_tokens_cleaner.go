package service

import (
	"context"
	"time"
)

const (
	tokenCleanerRate = 24 * time.Hour
)

func (s *Service) runRecoveryTokensCleaner() {
	log := s.app.Log.WithField("service", "recovery-tokens-cleaner")

	if s.app.Cfg.TokenCleanerDisabled {
		log.Warn("recovery tokens cleaner is disabled")
		return
	}

	recoveryTokensQ := s.app.RecoveryTokensQ.Clone(context.Background())

	go func() {
		ticker := time.NewTicker(tokenCleanerRate)
		defer ticker.Stop()

		for ; ; <-ticker.C {
			if err := recoveryTokensQ.DeleteExpiredTokens(); err != nil {
				log.WithError(err).Error("Failed to delete tokens")
			}
		}
	}()
}
