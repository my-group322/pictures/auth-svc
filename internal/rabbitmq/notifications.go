package rabbitmq

import (
	"encoding/json"

	"gitlab.com/my-group322/pictures/auth-svc/resources"

	"github.com/assembla/cony"
	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
)

//go:generate mockery --case underscore --name Publisher --outpkg publishermock

type Publisher interface {
	SendVerificationLink(email, name, link string)
}

type publisher struct {
	log *logrus.Logger
	cli *cony.Publisher
}

func NewPublisher(log *logrus.Logger, rabbitMQUrl string) Publisher {
	cli := cony.NewClient(
		cony.URL(rabbitMQUrl),
		cony.Backoff(cony.DefaultBackoff),
	)

	exc := cony.Exchange{
		Name:       "notifications",
		Kind:       "fanout",
		AutoDelete: true,
	}
	cli.Declare([]cony.Declaration{
		cony.DeclareExchange(exc),
	})

	pbl := cony.NewPublisher(exc.Name, "")
	cli.Publish(pbl)

	go func() {
		for cli.Loop() {
			if err := <-cli.Errors(); true {
				log.Error(err)
			}
		}
	}()

	return &publisher{
		log: log,
		cli: pbl,
	}
}

func (p publisher) SendVerificationLink(email, name, link string) {
	content, _ := json.Marshal(resources.DeliveryContent{
		Destination:  email,
		Link:         link,
		ReceiverName: name,
	})

	delivery := amqp.Publishing{
		ContentType: "text/plain",
		Body:        content,
	}

	err := p.cli.Publish(delivery)
	if err != nil {
		p.log.WithError(err).Error("failed to send notifiction")
	}
}
