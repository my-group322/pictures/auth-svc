package pkgdata

import (
	"github.com/jackc/pgconn"
	"github.com/pkg/errors"
	"gorm.io/gorm"
)

type dataError struct {
	message string
}

func (d dataError) Error() string {
	return d.message
}

var (
	ErrUniqueViolationError = dataError{message: "unique violation"}
	ErrNotAffected          = dataError{message: "not deleted"}
)

func FromGormError(err error) error {
	if err == nil {
		return nil
	}

	pgErr := err.(*pgconn.PgError)
	if pgErr.Code == "23505" {
		return errors.Wrap(ErrUniqueViolationError, err.Error())
	}

	return err
}

func ErrorFromDelete(db *gorm.DB) error {
	aff := db.RowsAffected

	if aff == 0 {
		if db.Error != nil {
			return errors.Wrap(ErrNotAffected, db.Error.Error())
		}

		return ErrNotAffected
	}

	return db.Error
}
