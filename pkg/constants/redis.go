package constants

const (
	RedisSessionsKey       = "active:sessions"
	RedisUsersKey          = "users"
	RedisLastPassChangeKey = "last_pass_change"
	RedisIsVerifiedKey     = "is_verified"
	RedisLastActiveSession = "active:sessions:last"
)
