package jwt

import "github.com/dgrijalva/jwt-go"

type CustomClaimsJwt struct {
	UserID    string `json:"user_id"`
	SessionID int64  `json:"session_id"`
	jwt.StandardClaims
}
