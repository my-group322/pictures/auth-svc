package renderer

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strconv"

	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/pkg/errors"
)

const mediaType = "application/vnd.api+json"

func marshalErrors(w io.Writer, errorObjects []*errorObject) error {
	return json.NewEncoder(w).Encode(&errorsPayload{Errors: errorObjects})
}

type errorsPayload struct {
	Errors []*errorObject `json:"errors"`
}

type errorObject struct {
	ID     string                  `json:"id,omitempty"`
	Title  string                  `json:"title,omitempty"`
	Detail string                  `json:"detail,omitempty"`
	Status string                  `json:"status,omitempty"`
	Code   string                  `json:"code,omitempty"`
	Meta   *map[string]interface{} `json:"meta,omitempty"`
}

func (e *errorObject) Error() string {
	return fmt.Sprintf("Error: %s %s\n", e.Title, e.Detail)
}

func RenderErr(w http.ResponseWriter, err ...*errorObject) {
	w.Header().Set("content-type", mediaType)

	status, perr := strconv.ParseInt(err[0].Status, 10, 64)
	if perr != nil {
		status = http.StatusInternalServerError
	}
	w.WriteHeader(int(status))

	_ = marshalErrors(w, err)
}

func RenderStatus(w http.ResponseWriter, status int) {
	w.Header().Set("content-type", mediaType)

	w.WriteHeader(status)
}

func NewJsonErr(err error) []*errorObject {
	status := castCode(err)
	errMessage := err.Error()

	switch m := errors.Cause(err).(type) {
	case validation.Errors:
		return validationToJSONErrors(m)
	}

	return []*errorObject{{
		Title:  http.StatusText(status),
		Status: fmt.Sprintf("%d", status),
		Code:   strconv.Itoa(status),
		Detail: errMessage,
		Meta: &map[string]interface{}{
			"error_code": errMessage,
		},
	}}
}

func BadRequestErr(err error) []*errorObject {
	switch m := errors.Cause(err).(type) {
	case validation.Errors:
		return validationToJSONErrors(m)
	}

	panic("Unknown bad request error")
}

func validationToJSONErrors(m map[string]error) []*errorObject {
	errs := make([]*errorObject, 0, len(m))
	for key, value := range m {
		errs = append(errs, &errorObject{
			Title:  http.StatusText(http.StatusBadRequest),
			Status: fmt.Sprintf("%d", http.StatusBadRequest),
			Meta: &map[string]interface{}{
				"field": key,
				"error": value.Error(),
			},
		})
	}

	return errs
}

type Error struct {
	StatusCode int
	Message    string
}

func (e Error) Error() string {
	return e.Message
}

func castCode(err error) int {
	e, ok := err.(Error)
	if !ok {
		return http.StatusInternalServerError
	}

	return e.StatusCode
}
