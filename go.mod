module gitlab.com/my-group322/pictures/auth-svc

go 1.16

require (
	github.com/alicebob/miniredis/v2 v2.16.1
	github.com/assembla/cony v0.3.2
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-chi/chi/v5 v5.0.7
	github.com/go-ozzo/ozzo-validation v3.6.0+incompatible
	github.com/go-redis/redis/v8 v8.11.4
	github.com/google/uuid v1.3.0
	github.com/ilyasiv2003/go-http-client v0.1.17
	github.com/ilyasiv2003/newrelic-context v0.0.0-20211203210028-6b947dffac58
	github.com/jackc/pgconn v1.10.1
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/newrelic/go-agent v3.15.1+incompatible
	github.com/pkg/errors v0.9.1
	github.com/rubenv/sql-migrate v0.0.0-20211023115951-9f02b1e13857
	github.com/sirupsen/logrus v1.8.1
	github.com/streadway/amqp v1.0.0
	github.com/stretchr/testify v1.7.0
	gitlab.com/my-group322/json-api-shared v0.0.0-20211208100130-285519047ba1
	gitlab.com/my-group322/pictures/pics-svc v0.0.0-20211208103954-fa99c74e200d
	golang.org/x/crypto v0.0.0-20211202192323-5770296d904e
	gorm.io/driver/postgres v1.2.3
	gorm.io/gorm v1.22.4
)
