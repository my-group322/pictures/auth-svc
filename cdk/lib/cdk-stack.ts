import * as cdk from '@aws-cdk/core'
import { CfnPublicRepository } from '@aws-cdk/aws-ecr'

export class CdkStack extends cdk.Stack {
  props: any

  constructor(scope: cdk.Construct, id: string, props: any, stackProps?: cdk.StackProps) {
    super(scope, id, stackProps)

    this.props = props

    new CfnPublicRepository(this, id, {
      repositoryName: id,
    })
  }
}
